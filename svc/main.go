package main

import (
	"log"
	"time"

	"github.com/streadway/amqp"
)

func main() {
	realMain()
}

func realMain() {
	conn, err := amqp.Dial("amqp://user:password@rmq:5672/")
	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	declareInfra(conn)

	log.Println("1...")
	log.Println("2...")
	log.Println("3...")
	log.Println("Go!!!")

	go consume("resize", conn)
	go consume("watermark", conn)
	for {
		log.Println("Wait...")
		time.Sleep(5 * time.Second)
		log.Println("Publish!")
		publish(conn)
	}

}

func publish(cn *amqp.Connection) {
	ch, err := cn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	defer ch.Close()

	if err := ch.Publish(
		"media_put", // exchange
		"",          // key
		false,       // mandatory
		false,       // immediate
		amqp.Publishing{
			Body: []byte("media was put"),
		}, // message
	); err != nil {
		log.Fatal(err)
	}
}

func consume(queue string, cn *amqp.Connection) {
	ch, err := cn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	defer ch.Close()

	dlvs, err := ch.Consume(
		queue,        // queue
		"1",          // consumer
		false,        // autoAck
		false,        // exclusive
		false,        // noLocal
		false,        // noWait
		amqp.Table{}, // args
	)
	if err != nil {
		log.Fatal(err)
	}

	for dlv := range dlvs {
		log.Println(string(dlv.Body))
		dlv.Ack(false)
	}
}

func declareInfra(cn *amqp.Connection) {
	ch, err := cn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	defer ch.Close()

	if err := ch.ExchangeDeclare(
		"media_put",         // name
		amqp.ExchangeFanout, // kind
		true,                // durable
		false,               // auto-delete
		false,               // internal
		false,               // no-wait
		nil,                 // args
	); err != nil {
		log.Fatalf("exchange.declare: %s", err)
	}

	if _, err = ch.QueueDeclare(
		"resize", // name
		true,     // durable
		false,    // auto-delete
		false,    // exclusive
		false,    // no-wait
		nil,      // args
	); err != nil {
		log.Fatalf("queue.declare: %v", err)
	}

	if err = ch.QueueBind(
		"resize",    // name
		"",          // key
		"media_put", // exchange
		false,       // no-wait
		nil,         // args
	); err != nil {
		log.Fatalf("queue.bind: %v", err)
	}

	if _, err = ch.QueueDeclare(
		"watermark", // name
		true,        // durable
		false,       // auto-delete
		false,       // exclusive
		false,       // no-wait
		nil,         // args
	); err != nil {
		log.Fatalf("queue.declare: %v", err)
	}

	if err = ch.QueueBind(
		"watermark", // name
		"",          // key
		"media_put", // exchange
		false,       // no-wait
		nil,         // args
	); err != nil {
		log.Fatalf("queue.bind: %v", err)
	}
}
